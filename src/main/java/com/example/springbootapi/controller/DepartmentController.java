package com.example.springbootapi.controller;

import com.example.springbootapi.Service.DepartmentService;
import com.example.springbootapi.entity.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;
    @PostMapping("/departments")
    public Department saveDepartment(@RequestBody Department department) {

        return departmentService.saveDepartment(department);
    }
    @GetMapping("/department")
    public List<Department>fetchDepartmentList(){
        return departmentService.fetchDepartmentList();

    }
    @GetMapping("/department/{Id}")
    public Department fetchDepartmentListId(@PathVariable("Id") Long departmentID) {
       return departmentService.fetchDepartmentListId(departmentID);
    }
    @DeleteMapping("/department/{Id}")
    public String fetchDepartmentdeleteId(@PathVariable("Id") Long departmentID){
        departmentService.fetchDepartmentdeleteId(departmentID);
        return "deleted sucessfully";
    }
    @PutMapping("/department/{Id}")
    public Department updatedepartment(@PathVariable("Id") Long departmentID,
                                       @RequestBody Department department){
        return departmentService.updatedepartment(departmentID,department);
    }

}
