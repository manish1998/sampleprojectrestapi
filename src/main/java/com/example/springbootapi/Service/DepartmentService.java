package com.example.springbootapi.Service;

import com.example.springbootapi.entity.Department;

import java.util.AbstractCollection;
import java.util.List;

public interface DepartmentService {
    Department saveDepartment(Department department);

   public List<Department> fetchDepartmentList();

   public Department fetchDepartmentListId(Long Id);

   public void fetchDepartmentdeleteId(Long departmentID);

   public Department updatedepartment(Long departmentID, Department department);
}
