package com.example.springbootapi.Service;

import com.example.springbootapi.Repository.DepartmentRepository;
import com.example.springbootapi.entity.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class  DepartmentServiceImp implements DepartmentService{
@Autowired
   private DepartmentRepository departmentRepository;

    @Override
    public Department saveDepartment(Department department) {
        return departmentRepository.save(department);
    }

    @Override
    public List<Department> fetchDepartmentList() {
        return departmentRepository.findAll();
    }

    public Department fetchDepartmentListId(Long departmentID) {
        return departmentRepository.findById(departmentID).get();
    }

    @Override
    public void fetchDepartmentdeleteId(Long departmentID) {
        departmentRepository.deleteById(departmentID);
    }

    @Override
    public Department updatedepartment(Long departmentID, Department department) {
        Department debd = departmentRepository.findById(departmentID).get();
        if(Objects.nonNull(department.getDepartmentName())&&
                !"".equalsIgnoreCase(department.getDepartmentName())){
    debd.setDepartmentName(department.getDepartmentName());

        }
        if(Objects.nonNull(department.getDepartmentCode()) &&
                !"".equalsIgnoreCase(department.getDepartmentCode())) {
            debd.setDepartmentCode(department.getDepartmentCode());
        }

        if(Objects.nonNull(department.getDepartmentAddress()) &&
                !"".equalsIgnoreCase(department.getDepartmentAddress())) {
            debd.setDepartmentAddress(department.getDepartmentAddress());
        }

        return departmentRepository.save(debd);
    }


}
